package finance

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type InterestRate float64

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (c *InterestRate) UnmarshalGQL(v interface{}) error {
	switch v := v.(type) {
	case string:
		fv, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return err
		}
		*c = InterestRate(fv)
		return nil
	case float64:
		*c = InterestRate(v)
		return nil
	case int64:
		*c = InterestRate(v)
		return nil
	case int32:
		*c = InterestRate(v)
		return nil
	case int:
		*c = InterestRate(v)
		return nil
	case InterestRate:
		*c = v
		return nil
	case json.Number:
		fv, err := strconv.ParseFloat(string(v), 64)
		if err != nil {
			return err
		}
		*c = InterestRate(fv)
		return nil
	default:
		return fmt.Errorf("%T is not a InterestRate", v)
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (c InterestRate) MarshalGQL(w io.Writer) {
	io.WriteString(w, strconv.FormatFloat(float64(c), 'f', 4, 64))
}
