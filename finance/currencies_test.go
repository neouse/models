package finance

import (
	"bytes"
	"encoding/json"
	"testing"
)

func TestCurrencyAUD_UnmarshalGQL(t *testing.T) {
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		o       CurrencyAUD
		args    args
		wantRes CurrencyAUD
		wantErr bool
	}{
		{"string", CurrencyAUD(0), args{"0400300200.0000"}, 400300200, false},
		{"float64", CurrencyAUD(0), args{1.23}, 1.23, false},
		{"int64", CurrencyAUD(0), args{int64(123)}, 123, false},
		{"int32", CurrencyAUD(0), args{int32(123)}, 123, false},
		{"int", CurrencyAUD(0), args{int(123)}, 123, false},
		{"jsonNumber float", CurrencyAUD(0), args{json.Number("1.23")}, 1.23, false},
		{"jsonNumber int", CurrencyAUD(0), args{json.Number("123")}, 123, false},
		{"currency", CurrencyAUD(0), args{CurrencyAUD(123)}, 123, false},
		{"bad format jsonNumber", CurrencyAUD(0), args{json.Number("1..23")}, 0.0, true},
		{"bad format string", CurrencyAUD(0), args{"1..23"}, 0.0, true},
		{"bad type", CurrencyAUD(0), args{true}, 0.0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.UnmarshalGQL(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("CurrencyAUD.UnmarshalGQL() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.o != tt.wantRes {
				t.Errorf("CurrencyAUD.UnmarshalGQL() o = %v, wantRes %v", tt.o, tt.wantRes)
			}
		})
	}
}

func TestCurrencyAUD_MarshalGQL(t *testing.T) {
	tests := []struct {
		name  string
		c     CurrencyAUD
		wantW string
	}{
		{"padded", CurrencyAUD(1.2), "1.20"},
		{"small", CurrencyAUD(1.23), "1.23"},
		{"negative small", CurrencyAUD(-1.23), "-1.23"},
		{"large", CurrencyAUD(100000000000000.23), "100000000000000.23"},
		{"toolarge", CurrencyAUD(10000000000000000.23), "10000000000000000.00"},
		{"int", CurrencyAUD(1), "1.00"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			tt.c.MarshalGQL(w)
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("CurrencyAUD.MarshalGQL() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
