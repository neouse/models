package finance

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type CurrencyAUD float64

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (c *CurrencyAUD) UnmarshalGQL(v interface{}) error {
	switch v := v.(type) {
	case string:
		fv, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return err
		}
		*c = CurrencyAUD(fv)
		return nil
	case float64:
		*c = CurrencyAUD(v)
		return nil
	case int64:
		*c = CurrencyAUD(v)
		return nil
	case int32:
		*c = CurrencyAUD(v)
		return nil
	case int:
		*c = CurrencyAUD(v)
		return nil
	case CurrencyAUD:
		*c = v
		return nil
	case json.Number:
		fv, err := strconv.ParseFloat(string(v), 64)
		if err != nil {
			return err
		}
		*c = CurrencyAUD(fv)
		return nil
	default:
		return fmt.Errorf("%T is not a CurrencyAUD", v)
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (c CurrencyAUD) MarshalGQL(w io.Writer) {
	io.WriteString(w, strconv.FormatFloat(float64(c), 'f', 2, 64))
}
