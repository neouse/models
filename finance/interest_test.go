package finance

import (
	"bytes"
	"encoding/json"
	"testing"
)

func TestInterestRate_UnmarshalGQL(t *testing.T) {
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		o       InterestRate
		args    args
		wantRes InterestRate
		wantErr bool
	}{
		{"string", InterestRate(0), args{"0400300200.0000"}, 400300200, false},
		{"float64", InterestRate(0), args{1.23}, 1.23, false},
		{"negative float64", InterestRate(0), args{-1.23}, -1.23, false},
		{"int64", InterestRate(0), args{int64(123)}, 123, false},
		{"int32", InterestRate(0), args{int32(123)}, 123, false},
		{"int", InterestRate(0), args{int(123)}, 123, false},
		{"jsonNumber float", InterestRate(0), args{json.Number("1.23")}, 1.23, false},
		{"jsonNumber int", InterestRate(0), args{json.Number("123")}, 123, false},
		{"currency", InterestRate(0), args{InterestRate(123)}, 123, false},
		{"bad format jsonNumber", InterestRate(0), args{json.Number("1..23")}, 0.0, true},
		{"bad format string", InterestRate(0), args{"1..23"}, 0.0, true},
		{"bad type", InterestRate(0), args{true}, 0.0, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.UnmarshalGQL(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("InterestRate.UnmarshalGQL() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.o != tt.wantRes {
				t.Errorf("InterestRate.UnmarshalGQL() o = %v, wantRes %v", tt.o, tt.wantRes)
			}
		})
	}
}

func TestInterestRate_MarshalGQL(t *testing.T) {
	tests := []struct {
		name  string
		c     InterestRate
		wantW string
	}{
		{"padded", InterestRate(1.23), "1.2300"},
		{"small", InterestRate(1.23), "1.2300"},
		{"negative small", InterestRate(-1.0023), "-1.0023"},
		{"large", InterestRate(1000000000000.0023), "1000000000000.0023"},
		{"toolarge", InterestRate(100000000000000.0023), "100000000000000.0000"},
		{"int", InterestRate(1), "1.0000"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			tt.c.MarshalGQL(w)
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("InterestRate.MarshalGQL() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
