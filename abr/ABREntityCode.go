package abr

import (
	"fmt"
	"io"
	"strconv"
)

type ABREntityCode string

const (
	// Approved Deposit Fund
	ABREntityCodeAdf ABREntityCode = "ADF"
	// APRA Regulated Fund (Fund Type Unknown)
	ABREntityCodeArf ABREntityCode = "ARF"
	// Commonwealth Government Public Company
	ABREntityCodeCcb ABREntityCode = "CCB"
	// Commonwealth Government Co-operative
	ABREntityCodeCcc ABREntityCode = "CCC"
	// Commonwealth Government Limited Partnership
	ABREntityCodeCcl ABREntityCode = "CCL"
	// Commonwealth Government Other Unincorporated Entity
	ABREntityCodeCcn ABREntityCode = "CCN"
	// Commonwealth Government Other Incorporated Entity
	ABREntityCodeCco ABREntityCode = "CCO"
	// Commonwealth Government Pooled Development Fund
	ABREntityCodeCcp ABREntityCode = "CCP"
	// Commonwealth Government Private Company
	ABREntityCodeCcr ABREntityCode = "CCR"
	// Commonwealth Government Strata Title
	ABREntityCodeCcs ABREntityCode = "CCS"
	// Commonwealth Government Public Trading Trust
	ABREntityCodeCct ABREntityCode = "CCT"
	// Commonwealth Government Corporate Unit Trust
	ABREntityCodeCcu ABREntityCode = "CCU"
	// Commonwealth Government Statutory Authority
	ABREntityCodeCga ABREntityCode = "CGA"
	// Commonwealth Government Company
	ABREntityCodeCgc ABREntityCode = "CGC"
	// Commonwealth Government Entity
	ABREntityCodeCge ABREntityCode = "CGE"
	// Commonwealth Government Partnership
	ABREntityCodeCgp ABREntityCode = "CGP"
	// Commonwealth Government Super Fund
	ABREntityCodeCgs ABREntityCode = "CGS"
	// Commonwealth Government Trust
	ABREntityCodeCgt ABREntityCode = "CGT"
	// Cash Management Trust
	ABREntityCodeCmt ABREntityCode = "CMT"
	// Co-operative
	ABREntityCodeCop ABREntityCode = "COP"
	// Commonwealth Government APRA Regulated Public Sector Fund
	ABREntityCodeCsa ABREntityCode = "CSA"
	// Commonwealth Government APRA Regulated Public Sector Scheme
	ABREntityCodeCsp ABREntityCode = "CSP"
	// Commonwealth Government Non-Regulated Super Fund
	ABREntityCodeCSS ABREntityCode = "CSS"
	// Commonwealth Government Cash Management Trust
	ABREntityCodeCtc ABREntityCode = "CTC"
	// Commonwealth Government Discretionary Services Management Trust
	ABREntityCodeCtd ABREntityCode = "CTD"
	// Commonwealth Government Fixed Trust
	ABREntityCodeCtf ABREntityCode = "CTF"
	// Commonwealth Government Hybrid Trust
	ABREntityCodeCth ABREntityCode = "CTH"
	// Commonwealth Government Discretionary Investment Trust
	ABREntityCodeCti ABREntityCode = "CTI"
	// Commonwealth Government Listed Public Unit Trust
	ABREntityCodeCtl ABREntityCode = "CTL"
	// Commonwealth Government Unlisted Public Unit Trust
	ABREntityCodeCtq ABREntityCode = "CTQ"
	// Commonwealth Government Discretionary Trading Trust
	ABREntityCodeCtt ABREntityCode = "CTT"
	// Commonwealth Government Fixed Unit Trust
	ABREntityCodeCtu ABREntityCode = "CTU"
	// Corporate Unit Trust
	ABREntityCodeCut ABREntityCode = "CUT"
	// Deceased Estate
	ABREntityCodeDes ABREntityCode = "DES"
	// Diplomatic/Consulate Body or High Commissioner
	ABREntityCodeDip ABREntityCode = "DIP"
	// Discretionary Investment Trust
	ABREntityCodeDit ABREntityCode = "DIT"
	// Discretionary Services Management Trust
	ABREntityCodeDst ABREntityCode = "DST"
	// Discretionary Trading Trust
	ABREntityCodeDtt ABREntityCode = "DTT"
	// First Home Saver Accounts Trust
	ABREntityCodeFhs ABREntityCode = "FHS"
	// Family Partnership
	ABREntityCodeFpt ABREntityCode = "FPT"
	// Fixed Unit Trust
	ABREntityCodeFut ABREntityCode = "FUT"
	// Fixed Trust
	ABREntityCodeFxt ABREntityCode = "FXT"
	// Hybrid Trust
	ABREntityCodeHyt ABREntityCode = "HYT"
	// Individual/Sole Trader
	ABREntityCodeInd ABREntityCode = "IND"
	// Local Government Public Company
	ABREntityCodeLcb ABREntityCode = "LCB"
	// Local Government Co-operative
	ABREntityCodeLcc ABREntityCode = "LCC"
	// Local Government Limited Partnership
	ABREntityCodeLcl ABREntityCode = "LCL"
	// Local Government Other Unincorporated Entity
	ABREntityCodeLcn ABREntityCode = "LCN"
	// Local Government Other Incorporated Entity
	ABREntityCodeLco ABREntityCode = "LCO"
	// Local Government Pooled Development Fund
	ABREntityCodeLcp ABREntityCode = "LCP"
	// Local Government Private Company
	ABREntityCodeLcr ABREntityCode = "LCR"
	// Local Government Strata Title
	ABREntityCodeLcs ABREntityCode = "LCS"
	// Local Government Public Trading Trust
	ABREntityCodeLct ABREntityCode = "LCT"
	// Local Government Corporate Unit Trust
	ABREntityCodeLcu ABREntityCode = "LCU"
	// Local Government Statutory Authority
	ABREntityCodeLga ABREntityCode = "LGA"
	// Local Government Company
	ABREntityCodeLgc ABREntityCode = "LGC"
	// Local Government Entity
	ABREntityCodeLge ABREntityCode = "LGE"
	// Local Government Partnership
	ABREntityCodeLgp ABREntityCode = "LGP"
	// Local Government Trust
	ABREntityCodeLgt ABREntityCode = "LGT"
	// Limited Partnership
	ABREntityCodeLpt ABREntityCode = "LPT"
	// Local Government APRA Regulated Public Sector Fund
	ABREntityCodeLsa ABREntityCode = "LSA"
	// Local Government APRA Regulated Public Sector Scheme
	ABREntityCodeLsp ABREntityCode = "LSP"
	// Local Government Non-Regulated Super Fund
	ABREntityCodeLss ABREntityCode = "LSS"
	// Local Government Cash Management Trust
	ABREntityCodeLtc ABREntityCode = "LTC"
	// Local Government Discretionary Services Management Trust
	ABREntityCodeLtd ABREntityCode = "LTD"
	// Local Government Fixed Trust
	ABREntityCodeLtf ABREntityCode = "LTF"
	// Local Government Hybrid Trust
	ABREntityCodeLth ABREntityCode = "LTH"
	// Local Government Discretionary Investment Trust
	ABREntityCodeLti ABREntityCode = "LTI"
	// Local Government Listed Public Unit Trust
	ABREntityCodeLtl ABREntityCode = "LTL"
	// Local Government Unlisted Public Unit Trust
	ABREntityCodeLtq ABREntityCode = "LTQ"
	// Local Government Discretionary Trading Trust
	ABREntityCodeLtt ABREntityCode = "LTT"
	// Local Government Fixed Unit Trust
	ABREntityCodeLtu ABREntityCode = "LTU"
	// APRA Regulated Non-Public Offer Fund
	ABREntityCodeNpf ABREntityCode = "NPF"
	// Non-Regulated Superannuation Fund
	ABREntityCodeNrf ABREntityCode = "NRF"
	// Other Incorporated Entity
	ABREntityCodeOie ABREntityCode = "OIE"
	// Pooled Development Fund
	ABREntityCodePdf ABREntityCode = "PDF"
	// APRA Regulated Public Offer Fund
	ABREntityCodePof ABREntityCode = "POF"
	// Unlisted Public Unit Trust
	ABREntityCodePqt ABREntityCode = "PQT"
	// Australian Private Company
	ABREntityCodePrv ABREntityCode = "PRV"
	// Pooled Superannuation Trust
	ABREntityCodePst ABREntityCode = "PST"
	// Other Partnership
	ABREntityCodePtr ABREntityCode = "PTR"
	// Public Trading trust
	ABREntityCodePtt ABREntityCode = "PTT"
	// Australian Public Company
	ABREntityCodePub ABREntityCode = "PUB"
	// Listed Public Unit Trust
	ABREntityCodePut ABREntityCode = "PUT"
	// Small APRA Regulated Fund
	ABREntityCodeSaf ABREntityCode = "SAF"
	// State Government Public Company
	ABREntityCodeScb ABREntityCode = "SCB"
	// State Government Co-operative
	ABREntityCodeScc ABREntityCode = "SCC"
	// State Government Limited Partnership
	ABREntityCodeScl ABREntityCode = "SCL"
	// State Government Other Unincorporated Entity
	ABREntityCodeScn ABREntityCode = "SCN"
	// State Government Other Incorporated Entity
	ABREntityCodeSco ABREntityCode = "SCO"
	// State Government Pooled Development Fund
	ABREntityCodeScp ABREntityCode = "SCP"
	// State Government Private Company
	ABREntityCodeScr ABREntityCode = "SCR"
	// State Government Strata Title
	ABREntityCodeScs ABREntityCode = "SCS"
	// State Government Public Trading Trust
	ABREntityCodeSct ABREntityCode = "SCT"
	// State Government Corporate Unit Trust
	ABREntityCodeScu ABREntityCode = "SCU"
	// State Government Statutory Authority
	ABREntityCodeSga ABREntityCode = "SGA"
	// State Government Company
	ABREntityCodeSgc ABREntityCode = "SGC"
	// State Government Entity
	ABREntityCodeSge ABREntityCode = "SGE"
	// State Government Partnership
	ABREntityCodeSgp ABREntityCode = "SGP"
	// State Government Trust
	ABREntityCodeSgt ABREntityCode = "SGT"
	// ATO Regulated Self-Managed Superannuation Fund
	ABREntityCodeSmf ABREntityCode = "SMF"
	// State Government APRA Regulated Public Sector Fund
	ABREntityCodeSsa ABREntityCode = "SSA"
	// State Government APRA Regulated Public Sector Scheme
	ABREntityCodeSsp ABREntityCode = "SSP"
	// State Government Non-Regulated Super Fund
	ABREntityCodeSss ABREntityCode = "SSS"
	// State Government Cash Management Trust
	ABREntityCodeStc ABREntityCode = "STC"
	// State Government Discretionary Services Management Trust
	ABREntityCodeStd ABREntityCode = "STD"
	// State Government Fixed Trust
	ABREntityCodeStf ABREntityCode = "STF"
	// State Government Hybrid Trust
	ABREntityCodeSth ABREntityCode = "STH"
	// State Government Discretionary Investment Trust
	ABREntityCodeSti ABREntityCode = "STI"
	// State Government Listed Public Unit Trust
	ABREntityCodeStl ABREntityCode = "STL"
	// State Government Unlisted Public Unit Trust
	ABREntityCodeStq ABREntityCode = "STQ"
	// Strata-title
	ABREntityCodeStr ABREntityCode = "STR"
	// State Government Discretionary Trading Trust
	ABREntityCodeStt ABREntityCode = "STT"
	// State Government Fixed Unit Trust
	ABREntityCodeStu ABREntityCode = "STU"
	// Super Fund
	ABREntityCodeSup ABREntityCode = "SUP"
	// Territory Government Public Company
	ABREntityCodeTcb ABREntityCode = "TCB"
	// Territory Government Co-operative
	ABREntityCodeTcc ABREntityCode = "TCC"
	// Territory Government Limited Partnership
	ABREntityCodeTcl ABREntityCode = "TCL"
	// Territory Government Other Unincorporated Entity
	ABREntityCodeTcn ABREntityCode = "TCN"
	// Territory Government Other Incorporated Entity
	ABREntityCodeTco ABREntityCode = "TCO"
	// Territory Government Pooled Development Fund
	ABREntityCodeTCP ABREntityCode = "TCP"
	// Territory Government Private Company
	ABREntityCodeTcr ABREntityCode = "TCR"
	// Territory Government Strata Title
	ABREntityCodeTcs ABREntityCode = "TCS"
	// Territory Government Public Trading Trust
	ABREntityCodeTct ABREntityCode = "TCT"
	// Territory Government Corporate Unit Trust
	ABREntityCodeTcu ABREntityCode = "TCU"
	// Territory Government Statutory Authority
	ABREntityCodeTga ABREntityCode = "TGA"
	// Territory Government Entity
	ABREntityCodeTge ABREntityCode = "TGE"
	// Territory Government Partnership
	ABREntityCodeTgp ABREntityCode = "TGP"
	// Territory Government Trust
	ABREntityCodeTgt ABREntityCode = "TGT"
	// Other trust
	ABREntityCodeTrt ABREntityCode = "TRT"
	// Territory Government APRA Regulated Public Sector Fund
	ABREntityCodeTsa ABREntityCode = "TSA"
	// Territory Government APRA Regulated Public Sector Scheme
	ABREntityCodeTsp ABREntityCode = "TSP"
	// Territory Government Non-Regulated Super Fund
	ABREntityCodeTss ABREntityCode = "TSS"
	// Territory Government Cash Management Trust
	ABREntityCodeTtc ABREntityCode = "TTC"
	// Territory Government Discretionary Services Management Trust
	ABREntityCodeTtd ABREntityCode = "TTD"
	// Territory Government Fixed Trust
	ABREntityCodeTtf ABREntityCode = "TTF"
	// Territory Government Hybrid Trust
	ABREntityCodeTth ABREntityCode = "TTH"
	// Territory Government Discretionary Investment Trust
	ABREntityCodeTti ABREntityCode = "TTI"
	// Territory Government Listed Public Unit Trust
	ABREntityCodeTTL ABREntityCode = "TTL"
	// Territory Government Unlisted Public Unit Trust
	ABREntityCodeTtq ABREntityCode = "TTQ"
	// Territory Government Discretionary Trading Trust
	ABREntityCodeTtt ABREntityCode = "TTT"
	// Territory Government Fixed Unit Trust
	ABREntityCodeTtu ABREntityCode = "TTU"
	// Other Unincorporated Entity
	ABREntityCodeUIE ABREntityCode = "UIE"
)

var AllABREntityCode = []ABREntityCode{
	ABREntityCodeAdf,
	ABREntityCodeArf,
	ABREntityCodeCcb,
	ABREntityCodeCcc,
	ABREntityCodeCcl,
	ABREntityCodeCcn,
	ABREntityCodeCco,
	ABREntityCodeCcp,
	ABREntityCodeCcr,
	ABREntityCodeCcs,
	ABREntityCodeCct,
	ABREntityCodeCcu,
	ABREntityCodeCga,
	ABREntityCodeCgc,
	ABREntityCodeCge,
	ABREntityCodeCgp,
	ABREntityCodeCgs,
	ABREntityCodeCgt,
	ABREntityCodeCmt,
	ABREntityCodeCop,
	ABREntityCodeCsa,
	ABREntityCodeCsp,
	ABREntityCodeCSS,
	ABREntityCodeCtc,
	ABREntityCodeCtd,
	ABREntityCodeCtf,
	ABREntityCodeCth,
	ABREntityCodeCti,
	ABREntityCodeCtl,
	ABREntityCodeCtq,
	ABREntityCodeCtt,
	ABREntityCodeCtu,
	ABREntityCodeCut,
	ABREntityCodeDes,
	ABREntityCodeDip,
	ABREntityCodeDit,
	ABREntityCodeDst,
	ABREntityCodeDtt,
	ABREntityCodeFhs,
	ABREntityCodeFpt,
	ABREntityCodeFut,
	ABREntityCodeFxt,
	ABREntityCodeHyt,
	ABREntityCodeInd,
	ABREntityCodeLcb,
	ABREntityCodeLcc,
	ABREntityCodeLcl,
	ABREntityCodeLcn,
	ABREntityCodeLco,
	ABREntityCodeLcp,
	ABREntityCodeLcr,
	ABREntityCodeLcs,
	ABREntityCodeLct,
	ABREntityCodeLcu,
	ABREntityCodeLga,
	ABREntityCodeLgc,
	ABREntityCodeLge,
	ABREntityCodeLgp,
	ABREntityCodeLgt,
	ABREntityCodeLpt,
	ABREntityCodeLsa,
	ABREntityCodeLsp,
	ABREntityCodeLss,
	ABREntityCodeLtc,
	ABREntityCodeLtd,
	ABREntityCodeLtf,
	ABREntityCodeLth,
	ABREntityCodeLti,
	ABREntityCodeLtl,
	ABREntityCodeLtq,
	ABREntityCodeLtt,
	ABREntityCodeLtu,
	ABREntityCodeNpf,
	ABREntityCodeNrf,
	ABREntityCodeOie,
	ABREntityCodePdf,
	ABREntityCodePof,
	ABREntityCodePqt,
	ABREntityCodePrv,
	ABREntityCodePst,
	ABREntityCodePtr,
	ABREntityCodePtt,
	ABREntityCodePub,
	ABREntityCodePut,
	ABREntityCodeSaf,
	ABREntityCodeScb,
	ABREntityCodeScc,
	ABREntityCodeScl,
	ABREntityCodeScn,
	ABREntityCodeSco,
	ABREntityCodeScp,
	ABREntityCodeScr,
	ABREntityCodeScs,
	ABREntityCodeSct,
	ABREntityCodeScu,
	ABREntityCodeSga,
	ABREntityCodeSgc,
	ABREntityCodeSge,
	ABREntityCodeSgp,
	ABREntityCodeSgt,
	ABREntityCodeSmf,
	ABREntityCodeSsa,
	ABREntityCodeSsp,
	ABREntityCodeSss,
	ABREntityCodeStc,
	ABREntityCodeStd,
	ABREntityCodeStf,
	ABREntityCodeSth,
	ABREntityCodeSti,
	ABREntityCodeStl,
	ABREntityCodeStq,
	ABREntityCodeStr,
	ABREntityCodeStt,
	ABREntityCodeStu,
	ABREntityCodeSup,
	ABREntityCodeTcb,
	ABREntityCodeTcc,
	ABREntityCodeTcl,
	ABREntityCodeTcn,
	ABREntityCodeTco,
	ABREntityCodeTCP,
	ABREntityCodeTcr,
	ABREntityCodeTcs,
	ABREntityCodeTct,
	ABREntityCodeTcu,
	ABREntityCodeTga,
	ABREntityCodeTge,
	ABREntityCodeTgp,
	ABREntityCodeTgt,
	ABREntityCodeTrt,
	ABREntityCodeTsa,
	ABREntityCodeTsp,
	ABREntityCodeTss,
	ABREntityCodeTtc,
	ABREntityCodeTtd,
	ABREntityCodeTtf,
	ABREntityCodeTth,
	ABREntityCodeTti,
	ABREntityCodeTTL,
	ABREntityCodeTtq,
	ABREntityCodeTtt,
	ABREntityCodeTtu,
	ABREntityCodeUIE,
}

func (e ABREntityCode) IsValid() bool {
	switch e {
	case ABREntityCodeAdf, ABREntityCodeArf, ABREntityCodeCcb, ABREntityCodeCcc, ABREntityCodeCcl, ABREntityCodeCcn, ABREntityCodeCco, ABREntityCodeCcp, ABREntityCodeCcr, ABREntityCodeCcs, ABREntityCodeCct, ABREntityCodeCcu, ABREntityCodeCga, ABREntityCodeCgc, ABREntityCodeCge, ABREntityCodeCgp, ABREntityCodeCgs, ABREntityCodeCgt, ABREntityCodeCmt, ABREntityCodeCop, ABREntityCodeCsa, ABREntityCodeCsp, ABREntityCodeCSS, ABREntityCodeCtc, ABREntityCodeCtd, ABREntityCodeCtf, ABREntityCodeCth, ABREntityCodeCti, ABREntityCodeCtl, ABREntityCodeCtq, ABREntityCodeCtt, ABREntityCodeCtu, ABREntityCodeCut, ABREntityCodeDes, ABREntityCodeDip, ABREntityCodeDit, ABREntityCodeDst, ABREntityCodeDtt, ABREntityCodeFhs, ABREntityCodeFpt, ABREntityCodeFut, ABREntityCodeFxt, ABREntityCodeHyt, ABREntityCodeInd, ABREntityCodeLcb, ABREntityCodeLcc, ABREntityCodeLcl, ABREntityCodeLcn, ABREntityCodeLco, ABREntityCodeLcp, ABREntityCodeLcr, ABREntityCodeLcs, ABREntityCodeLct, ABREntityCodeLcu, ABREntityCodeLga, ABREntityCodeLgc, ABREntityCodeLge, ABREntityCodeLgp, ABREntityCodeLgt, ABREntityCodeLpt, ABREntityCodeLsa, ABREntityCodeLsp, ABREntityCodeLss, ABREntityCodeLtc, ABREntityCodeLtd, ABREntityCodeLtf, ABREntityCodeLth, ABREntityCodeLti, ABREntityCodeLtl, ABREntityCodeLtq, ABREntityCodeLtt, ABREntityCodeLtu, ABREntityCodeNpf, ABREntityCodeNrf, ABREntityCodeOie, ABREntityCodePdf, ABREntityCodePof, ABREntityCodePqt, ABREntityCodePrv, ABREntityCodePst, ABREntityCodePtr, ABREntityCodePtt, ABREntityCodePub, ABREntityCodePut, ABREntityCodeSaf, ABREntityCodeScb, ABREntityCodeScc, ABREntityCodeScl, ABREntityCodeScn, ABREntityCodeSco, ABREntityCodeScp, ABREntityCodeScr, ABREntityCodeScs, ABREntityCodeSct, ABREntityCodeScu, ABREntityCodeSga, ABREntityCodeSgc, ABREntityCodeSge, ABREntityCodeSgp, ABREntityCodeSgt, ABREntityCodeSmf, ABREntityCodeSsa, ABREntityCodeSsp, ABREntityCodeSss, ABREntityCodeStc, ABREntityCodeStd, ABREntityCodeStf, ABREntityCodeSth, ABREntityCodeSti, ABREntityCodeStl, ABREntityCodeStq, ABREntityCodeStr, ABREntityCodeStt, ABREntityCodeStu, ABREntityCodeSup, ABREntityCodeTcb, ABREntityCodeTcc, ABREntityCodeTcl, ABREntityCodeTcn, ABREntityCodeTco, ABREntityCodeTCP, ABREntityCodeTcr, ABREntityCodeTcs, ABREntityCodeTct, ABREntityCodeTcu, ABREntityCodeTga, ABREntityCodeTge, ABREntityCodeTgp, ABREntityCodeTgt, ABREntityCodeTrt, ABREntityCodeTsa, ABREntityCodeTsp, ABREntityCodeTss, ABREntityCodeTtc, ABREntityCodeTtd, ABREntityCodeTtf, ABREntityCodeTth, ABREntityCodeTti, ABREntityCodeTTL, ABREntityCodeTtq, ABREntityCodeTtt, ABREntityCodeTtu, ABREntityCodeUIE:
		return true
	}
	return false
}

func (e ABREntityCode) String() string {
	return string(e)
}

func (e *ABREntityCode) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return fmt.Errorf("enums must be strings")
	}

	*e = ABREntityCode(str)
	if !e.IsValid() {
		return fmt.Errorf("%s is not a valid ABREntityCode", str)
	}
	return nil
}

func (e ABREntityCode) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(e.String()))
}
