package abr

import (
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
)

var (
	abnRe         = regexp.MustCompile(`\d{11}`)
	ErrInvalidABN = errors.New("ABN must be 11 digits")
)

type ABN string

func (abn ABN) IsValid() bool {
	return abnRe.Match([]byte(abn))
}

func (abn ABN) String() string {
	return string(abn)
}

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (abn *ABN) UnmarshalGQL(v interface{}) error {
	s, ok := v.(string)
	if !ok {
		return fmt.Errorf("ABN must be a string")
	}

	*abn = ABN(s)
	if !abn.IsValid() {
		return fmt.Errorf("%s is not a valid ABN", s)
	}
	return nil
}

// MarshalGQL implements the graphql.Marshaler interface
func (abn ABN) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(abn.String()))
}
