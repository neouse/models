package abr

import (
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
)

var (
	acnRe         = regexp.MustCompile(`\d{9}`)
	ErrInvalidACN = errors.New("ACN must be be 9 digits")
)

type ACN string

func (acn ACN) IsValid() bool {
	return acnRe.Match([]byte(acn))
}

func (acn ACN) String() string {
	return string(acn)
}

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (acn *ACN) UnmarshalGQL(v interface{}) error {
	s, ok := v.(string)
	if !ok {
		return fmt.Errorf("ACN must be a string")
	}

	*acn = ACN(s)
	if !acn.IsValid() {
		return fmt.Errorf("%s is not a valid ACN", s)
	}
	return nil
}

// MarshalGQL implements the graphql.Marshaler interface
func (acn ACN) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(acn.String()))
}
