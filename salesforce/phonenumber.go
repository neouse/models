package salesforce

import (
	"errors"
	"fmt"
	"io"
	"strconv"

	"github.com/nyaruka/phonenumbers"
)

var (
	// ErrNotString is returned when the phone number is not a string.
	ErrNotString = errors.New("Phone number must be a string")
)

// PhoneNumber is the Salesforce formatted phone number
type PhoneNumber string

// IsValid returns true when the phone number is valid for Australia otherwise false.
func (o PhoneNumber) IsValid() bool {
	phone, err := phonenumbers.Parse(o.String(), "AU")
	if err != nil {
		return false
	}
	return phonenumbers.IsValidNumber(phone)
}

func (o PhoneNumber) String() string {
	return string(o)
}

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (o *PhoneNumber) UnmarshalGQL(v interface{}) error {
	s, ok := v.(string)
	if !ok {
		return ErrNotString
	}

	phone, err := phonenumbers.Parse(s, "AU")
	if err != nil {
		return err
	}
	if !phonenumbers.IsValidNumber(phone) {
		return phonenumbers.ErrNotANumber
	}
	*o = PhoneNumber(fmt.Sprintf("%v%v", phone.GetCountryCode(), phone.GetNationalNumber()))
	return nil
}

// MarshalGQL implements the graphql.Marshaler interface
func (o PhoneNumber) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(o.String()))
}
