package salesforce

import (
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
)

var (
	dateRe = regexp.MustCompile(`\d{4}-\d\d-\d\d`)
	// ErrInvalidDate is returned when the date does not match the format YYYY-MM-DD where YYYY is the four digit year, MM is the 2 digit month and DD is the 2 digit day of the month.
	ErrInvalidDate = errors.New("Date must match YYYY-MM-DD")
	// ErrDateNotString is returned when the date is not a string.
	ErrDateNotString = errors.New("Date must be a string")
)

// Date is the Salesforce formatted date
type Date string

// IsValid returns true when the date is valid otherwise false.
func (o Date) IsValid() bool {
	return dateRe.Match([]byte(o))
}

func (o Date) String() string {
	return string(o)
}

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (o *Date) UnmarshalGQL(v interface{}) error {
	s, ok := v.(string)
	if !ok {
		return ErrDateNotString
	}

	*o = Date(s)
	if !o.IsValid() {
		return ErrInvalidDate
	}
	return nil
}

// MarshalGQL implements the graphql.Marshaler interface
func (o Date) MarshalGQL(w io.Writer) {
	fmt.Fprint(w, strconv.Quote(o.String()))
}
