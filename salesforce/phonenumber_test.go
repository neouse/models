package salesforce

import "testing"

func TestPhone_UnmarshalGQL(t *testing.T) {
	p := PhoneNumber("")
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		o       PhoneNumber
		args    args
		wantRes string
		wantErr bool
	}{
		{"good1", p, args{"+61400300200"}, "61400300200", false},
		{"good2", p, args{"61400300200"}, "61400300200", false},
		{"good3", p, args{"0400300200"}, "61400300200", false},
		{"good4", p, args{"0400 300 200"}, "61400300200", false},
		{"good5", p, args{"+61 0400 300 200"}, "61400300200", false},
		{"good6", p, args{"+61 400 300 200"}, "61400300200", false},
		{"bad1", p, args{"6140030200"}, "", true},
		{"bad2", p, args{6140030200}, "", true},
		{"bad3", p, args{"6x"}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.o.UnmarshalGQL(tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("Phone.UnmarshalGQL() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.o.String() != tt.wantRes {
				t.Errorf("Phone.UnmarshalGQL() o = %v, wantRes %v", tt.o, tt.wantRes)
			}
		})
	}
}
